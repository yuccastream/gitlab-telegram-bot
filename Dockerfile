FROM golang:1.19-buster as build
WORKDIR /go/src/gitlab.com/yuccastream/gitlab-telegram-bot/

COPY . ./
RUN ./build.sh bin

FROM debian:10-slim as release
LABEL io.yucca.gitlab-telegram-bot.image=true
WORKDIR /opt/gitlab-telegram-bot

RUN apt-get update \
    && apt-get install -y ca-certificates \
# Remove trash
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
COPY --from=build /go/src/gitlab.com/yuccastream/gitlab-telegram-bot/gitlab-telegram-bot /opt/gitlab-telegram-bot/gitlab-telegram-bot

EXPOSE 3000
ENTRYPOINT [ "/opt/gitlab-telegram-bot/gitlab-telegram-bot" ]
CMD [ "-h" ]
