# Telegram Bot for GitLab

## Configuration

Telegram ChatID must be passed with a query argument `chat_id`.

```yaml
---
token: AABB:CCDD  # can be passed with a environment variable `TOKEN`
secret: secret
listen: 0.0.0.0:3000
users_blocked:
    - username
pipeline_branches:
    - master
pipeline_statuses:
    - failed
templates:
    merge_request:
        fromText: |
            🔀 <a href="{{.ObjectAttributes.URL}}">{{.Project.PathWithNamespace}}!{{.ObjectAttributes.IID}}</a> <b>{{.ObjectAttributes.Title}}</b>
            {{.ObjectAttributes.Action | cap}} by <a href="https://gitlab.com/{{.User.Username}}">{{.User.Name}}</a>
    issue:
        fromText: |
            🎫 <a href="{{.ObjectAttributes.URL}}">{{.Project.PathWithNamespace}}#{{.ObjectAttributes.IID}}</a> <b>{{.ObjectAttributes.Title}}</b>
            {{.ObjectAttributes.Action | cap}} by <a href="https://gitlab.com/{{.User.Username}}">{{.User.Name}}</a>
    merge_request_note:
        fromText: |
            💬 <a href="{{ .ObjectAttributes.URL }}">{{ .Project.PathWithNamespace }}!{{ .MergeRequest.IID }}</a> <b>{{ .MergeRequest.Title }}</b>
            {{ .ObjectAttributes.Note }}
            – <a href="https://gitlab.com/{{.User.Username}}">{{ .User.Name }}</a>
    issue_note:
        fromText: |
            💬 <a href="{{ .ObjectAttributes.URL }}">{{ .Project.PathWithNamespace }}#{{ .Issue.IID }}</a> <b>{{ .Issue.Title }}</b>
            {{ .ObjectAttributes.Note }}
            – <a href="https://gitlab.com/{{.User.Username}}">{{ .User.Name }}</a>
    pipeline:
        fromText: |
            🆕 <a href="{{.Project.WebURL}}/-/pipelines/{{.ObjectAttributes.ID}}">{{.Project.PathWithNamespace}}</a> <b>Pipeline {{.ObjectAttributes.Status}}</b>
            – <a href="https://gitlab.com/{{.User.Username}}">{{.User.Name}}</a>
```

## Links

* [GitLab Documentation. Webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)
