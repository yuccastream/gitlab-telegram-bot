package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/microcosm-cc/bluemonday"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

type Bot struct {
	mux    *http.ServeMux
	config *Config
	cli    Telegram
	policy *bluemonday.Policy
}

func New(config *Config) (*Bot, error) {
	b := &Bot{
		cli:    NewBotAPIDryRun(),
		config: config,
	}

	if !config.DryRun {
		telegram, err := tgbotapi.NewBotAPI(config.Token)
		if err != nil {
			return nil, err
		}
		b.cli = telegram
	}

	p := bluemonday.NewPolicy()
	// https://core.telegram.org/bots/api#html-style
	allowedElements := []string{"br",
		"a",
		"u", "ins",
		"b", "strong",
		"i", "em",
		"s", "strike", "del",
		"span", "tg-spoiler",
		"code",
		"pre",
	}
	skipContentElements := []string{
		"details",
		"summary",
	}
	p.AllowElements(allowedElements...)
	p.SkipElementsContent(skipContentElements...)
	p.AllowAttrs("href").Globally()
	b.policy = p

	mux := http.NewServeMux()
	mux.HandleFunc("/telegram", b.EventHandler)
	b.mux = mux

	return b, nil
}

func (b *Bot) validateWebhook(r *http.Request) error {
	if r.Method != http.MethodPost {
		return errors.New("incorrect request: check HTTP method")
	}
	if r.Header.Get("X-Gitlab-Event") == "" {
		return errors.New("incorrect request: check X-Gitlab-Event")
	}
	if len(b.config.Secret) > 0 && r.Header.Get("X-Gitlab-Token") != b.config.Secret {
		return errors.New("incorrect request: check X-Gitlab-Token")
	}
	return nil
}

func (b *Bot) isUserBlocked(username string) bool {
	for _, name := range b.config.BlockedUsers {
		if strings.ToLower(username) == strings.ToLower(name) {
			return true
		}
	}
	return false
}

func (b *Bot) isBlockerPipeline(event *gitlab.PipelineEvent) bool {
	matchBranch := len(b.config.PipelineBranches) == 0
	for _, branch := range b.config.PipelineBranches {
		if event.ObjectAttributes.Ref == branch {
			matchBranch = true
			break
		}
	}

	matchStatus := len(b.config.PipelineStatuses) == 0
	for _, status := range b.config.PipelineStatuses {
		if event.ObjectAttributes.Status == status {
			matchStatus = true
			break
		}
	}

	return !matchBranch || !matchStatus
}

func (b *Bot) makeMessage(templateName string, v interface{}) (string, error) {
	tmpl, ok := b.config.Templates[templateName]
	if !ok {
		return "", nil
	}
	tmpl = strings.Replace(tmpl, "\n\n", "\n", -1)
	tmpl = strings.TrimSpace(tmpl)
	return GenerateTemplate(templateName, tmpl, v)
}

func (b *Bot) EventHandler(w http.ResponseWriter, r *http.Request) {
	chatIDRaw := r.URL.Query().Get("chat_id")
	if chatIDRaw == "" {
		http.Error(w, "Empty chat id", http.StatusBadRequest)
		return
	}
	chatID, err := strconv.ParseInt(chatIDRaw, 10, 64)
	if err != nil {
		http.Error(w, fmt.Sprintf("Incorrect chat id: %v", err), http.StatusBadRequest)
		return
	}

	if err := b.validateWebhook(r); err != nil {
		logrus.WithError(err).Error("Webhook validation failed")
		http.Error(w, "Webhook validation failed", http.StatusBadRequest)
		return
	}

	payload, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.WithError(err).Error("Failed to read request body")
		http.Error(w, "Failed to read request body", http.StatusInternalServerError)
		return
	}

	eventType := gitlab.WebhookEventType(r)
	logrus.Infof("%s handled", eventType)
	logrus.Debugf("%s body: %s", eventType, string(payload))

	event, err := gitlab.ParseWebhook(eventType, payload)
	if err != nil {
		logrus.WithError(err).WithField("event", eventType).
			Error("Failed to parse request")
		http.Error(w, "Failed to parse request", http.StatusBadRequest)
		return
	}

	var (
		message      string
		username     string
		wipUpdate    bool
		wipNote      bool
		eventData    interface{}
		templateName = TemplateNameFromEventType(eventType)
	)

	eventData = event
	switch event := event.(type) {
	case *gitlab.PushEvent:
		username = event.UserUsername
	case *gitlab.TagEvent:
		username = event.UserName
		templateName = "tag"
	case *gitlab.IssueEvent:
		username = event.User.Username
		e := IssueEventWrapped(*event)
		eventData = &e
	case *gitlab.JobEvent:
		username = event.User.Name
	case *gitlab.CommitCommentEvent:
		username = event.User.Username
		templateName = "commit_note"
	case *gitlab.MergeCommentEvent:
		username = event.User.Username
		wipNote = event.MergeRequest.WorkInProgress
		templateName = "merge_request_note"
	case *gitlab.IssueCommentEvent:
		username = event.User.Username
		templateName = "issue_note"
	case *gitlab.SnippetCommentEvent:
		username = event.User.Username
		templateName = "snippet_note"
	case *gitlab.MergeEvent:
		username = event.User.Username
		wipUpdate = event.ObjectAttributes.WorkInProgress
		e := MergeEventWrapped(*event)
		eventData = &e
	case *gitlab.WikiPageEvent:
		username = event.User.Username
	case *gitlab.PipelineEvent:
		username = event.User.Username
		if b.isBlockerPipeline(event) {
			logrus.Debugf("%s skipped by pipeline config", eventType)
			return
		}
	case *gitlab.BuildEvent:
		username = event.User.Name
	}

	if b.isUserBlocked(username) {
		logrus.Debugf("%s skipped by users_blocked %s", eventType, username)
		return
	}

	if b.config.SkipWIP && (wipUpdate || wipNote) {
		logrus.Debugf("%s skipped by skip_wip", eventType)
		return
	}

	if b.config.SkipWIPUpdates && wipUpdate {
		logrus.Debugf("%s skipped by skip_wip_updates", eventType)
		return
	}

	if b.config.SkipWIPNotes && wipNote {
		logrus.Debugf("%s skipped by skip_wip_notes", eventType)
		return
	}

	message, err = b.makeMessage(templateName, eventData)
	if err != nil {
		logrus.WithError(err).WithField("template", templateName).
			Error("Error making message")
		http.Error(w, "Error making message", http.StatusInternalServerError)
		return
	}
	if len(message) == 0 {
		logrus.WithField("template", templateName).
			Debugf("%s skipped by empty message", eventType)
		return
	}

	message = b.policy.Sanitize(message)
	tMessage := tgbotapi.NewMessage(chatID, message)
	tMessage.DisableWebPagePreview = true
	tMessage.ParseMode = tgbotapi.ModeHTML
	_, err = b.cli.Send(tMessage)
	if err != nil {
		logrus.WithError(err).WithField("message", message).
			Error("Failed to send message")
		http.Error(w, "Failed to send message", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Telegram-Chat-Id", strconv.FormatInt(chatID, 10))
	w.WriteHeader(http.StatusCreated)
}

func (b *Bot) Run() error {
	return http.ListenAndServe(b.config.Listen, b.mux)
}
