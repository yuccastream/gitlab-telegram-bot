package main

import (
	"encoding/json"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type BotAPIDryRun struct {
	err      error
	messages []string
}

func NewBotAPIDryRun() *BotAPIDryRun {
	return &BotAPIDryRun{}
}

func (b *BotAPIDryRun) Send(c tgbotapi.Chattable) (tgbotapi.Message, error) {
	vars := make(map[string]string)
	body, _ := json.Marshal(c)
	_ = json.Unmarshal(body, &vars)
	text, ok := vars["Text"]
	if ok {
		b.messages = append(b.messages, text)
	}
	return tgbotapi.Message{}, b.err
}
