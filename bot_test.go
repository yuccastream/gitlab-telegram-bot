package main

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBot_EventHandler(t *testing.T) {
	config, err := LoadConfig("test_data/valid.yaml")
	if !assert.NoError(t, err) {
		return
	}
	config.DryRun = true

	bot, err := New(config)
	if !assert.NoError(t, err) {
		return
	}
	telegram := NewBotAPIDryRun()
	bot.cli = telegram

	s := httptest.NewServer(http.HandlerFunc(bot.EventHandler))
	defer s.Close()

	resp, _ := http.Get(s.URL + "?chat_id=11")
	b, _ := io.ReadAll(resp.Body)
	resp.Body.Close()
	assert.Contains(t, string(b), "Webhook validation failed")

	cli := http.Client{}
	req, _ := http.NewRequest(http.MethodPost, s.URL+"?chat_id=99", nil)
	req.Header.Set("X-Gitlab-Event", "Merge Request Hook")
	resp, _ = cli.Do(req)
	b, _ = io.ReadAll(resp.Body)
	resp.Body.Close()
	assert.Contains(t, string(b), "Failed to parse request")

	config.BlockedUsers = []string{"root"}
	f, err := os.Open("test_data/events/merge_request_hook.json")
	req, _ = http.NewRequest(http.MethodPost, s.URL, f)
	req.Header.Set("X-Gitlab-Event", "Merge Request Hook")
	resp, _ = cli.Do(req)
	b, _ = io.ReadAll(resp.Body)
	resp.Body.Close()
	assert.Contains(t, string(b), "")
	assert.Equal(t, 0, len(telegram.messages))

	config.SkipWIP = true
	config.BlockedUsers = []string{}
	f, err = os.Open("test_data/events/merge_request_hook.json")
	req, _ = http.NewRequest(http.MethodPost, s.URL, f)
	req.Header.Set("X-Gitlab-Event", "Merge Request Hook")
	resp, _ = cli.Do(req)
	b, _ = io.ReadAll(resp.Body)
	resp.Body.Close()
	assert.Contains(t, string(b), "")
	assert.Equal(t, 0, len(telegram.messages))

	config.SkipWIP = false
	config.BlockedUsers = []string{}

	tests := []struct {
		event    string
		filename string
		chatID   int64
	}{
		{
			event:    "Merge Request Hook",
			filename: "merge_request_hook",
			chatID:   2,
		},
		{
			event:    "Issue Hook",
			filename: "issue_hook",
			chatID:   10,
		},
		{
			event:    "Note Hook",
			filename: "merge_request_note_hook",
			chatID:   13,
		},
		{
			event:    "Note Hook",
			filename: "issue_note_hook",
			chatID:   1,
		},
		{
			event:    "Note Hook",
			filename: "commit_note_hook",
			chatID:   999,
		},
		{
			event:    "Pipeline Hook",
			filename: "pipeline_hook",
			chatID:   101010,
		},
		{
			event:    "Tag Push Hook",
			filename: "tag_push_hook",
		},
	}
	for _, test := range tests {
		t.Logf("Checking %s template", test.filename)
		f, err = os.Open(fmt.Sprintf("test_data/events/%s.json", test.filename))
		if !assert.NoError(t, err) {
			return
		}
		handlerURL := s.URL + fmt.Sprintf("?chat_id=%d", test.chatID)
		req, _ = http.NewRequest(http.MethodPost, handlerURL, f)
		req.Header.Set("X-Gitlab-Event", test.event)
		resp, _ = cli.Do(req)
		b, err = io.ReadAll(resp.Body)
		if !assert.NoError(t, err) {
			return
		}
		resp.Body.Close()
		if !assert.Contains(t, string(b), "") {
			return
		}
		assert.Equal(t, strconv.FormatInt(test.chatID, 10), resp.Header.Get("Telegram-Chat-Id"))
		body, err := os.ReadFile(fmt.Sprintf("test_data/events/%s.html", test.filename))
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, string(body), telegram.messages[len(telegram.messages)-1])
	}
}
