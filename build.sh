#!/usr/bin/env bash

set -e

DEBUG=${DEBUG:-false}
if ${DEBUG}; then
    set -x
fi
SCRIPT_DIR="$(dirname $(readlink -f $0))"
cd ${SCRIPT_DIR}

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/yuccastream/gitlab-telegram-bot'}
CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}

_help() {
echo -e "help menu:
bin     - Build binary file
docker  - Build docker image
"
}

_docker() {
    docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -f ${SCRIPT_DIR}/Dockerfile ${SCRIPT_DIR}
    docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
}

_bin() {
    go build -o gitlab-telegram-bot *.go
}


case ${1} in
    bin) _bin ;;
    docker) _docker ;;
    *) _help ;;
esac
