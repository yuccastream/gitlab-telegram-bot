package main

import (
	"errors"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	BlockedUsers     []string `yaml:"users_blocked"`
	DryRun           bool     `yaml:"dry_run"`
	Listen           string   `yaml:"listen"`
	PipelineBranches []string `yaml:"pipeline_branches"`
	// PipelineStatuses:
	// * created
	// * waiting_for_resource
	// * preparing
	// * pending
	// * running
	// * success
	// * failed
	// * canceled
	// * skipped
	// * manual
	// * scheduled
	PipelineStatuses []string `yaml:"pipeline_statuses"`
	Secret           string   `yaml:"secret"`
	SkipWIP          bool     `yaml:"skip_wip"`
	SkipWIPNotes     bool     `yaml:"skip_wip_notes"`
	SkipWIPUpdates   bool     `yaml:"skip_wip_updates"`
	Token            string   `yaml:"token"`

	TemplatesRaw map[string]Template `yaml:"templates"`
	Templates    map[string]string   `yaml:"-"`
}

type Template struct {
	File string `yaml:"fromFile"`
	Text string `yaml:"fromText"`
}

func LoadConfig(confFile string) (*Config, error) {
	data, err := os.ReadFile(confFile)
	if err != nil {
		return nil, err
	}
	config := &Config{
		Templates: make(map[string]string),
	}
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	for name, tmpl := range config.TemplatesRaw {
		if len(tmpl.Text) > 0 {
			config.Templates[name] = tmpl.Text
		} else if len(tmpl.File) > 0 {
			b, err := os.ReadFile(tmpl.File)
			if err != nil {
				return nil, err
			}
			config.Templates[name] = string(b)
		}
	}
	if len(config.Templates) == 0 {
		return nil, errors.New("empty templates map")
	}
	if token := os.Getenv("TOKEN"); len(token) > 0 {
		config.Token = token
	}
	if secret := os.Getenv("SECRET"); len(secret) > 0 {
		config.Secret = secret
	}
	return config, nil
}
