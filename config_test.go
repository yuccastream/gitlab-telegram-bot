package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	_, err := LoadConfig("test_data/not_found.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "no such file or directory")
	}

	_, err = LoadConfig("test_data/incorrect_syntax.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "cannot unmarshal")
	}

	_, err = LoadConfig("test_data/empty_templates_map.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "empty templates map")
	}

	_, err = LoadConfig("test_data/valid.yaml")
	assert.NoError(t, err)
}
