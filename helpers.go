package main

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/xanzy/go-gitlab"
)

var (
	funcMap = template.FuncMap{
		"cap": strings.Title,
	}
)

func GenerateTemplate(name, tmpl string, data interface{}) (string, error) {
	var templateEng *template.Template
	buf := bytes.NewBufferString("")
	templateEng = template.New(name).Funcs(funcMap)
	if messageTmpl, err := templateEng.Parse(tmpl); err != nil {
		return "", fmt.Errorf("failed to parse template: %v", err)
	} else if err := messageTmpl.Execute(buf, data); err != nil {
		return "", fmt.Errorf("failed to execute template: %v", err)
	}
	return buf.String(), nil
}

func TemplateNameFromEventType(eventType gitlab.EventType) string {
	templateName := strings.ToLower(string(eventType))
	templateName = strings.Replace(templateName, " ", "_", -1)
	templateName = strings.Replace(templateName, "_hook", "", 1)
	return templateName
}
