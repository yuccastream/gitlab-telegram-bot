package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateTemplate(t *testing.T) {
	_, err := GenerateTemplate("text", "{{", nil)
	assert.Error(t, err)

	_, err = GenerateTemplate("text", "{{ .foobar }}", map[string]string{
		"foobar": "barfoo",
	})
	assert.NoError(t, err)
}
