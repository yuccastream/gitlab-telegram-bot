package main

import (
	"flag"
	"log"

	"github.com/sirupsen/logrus"
)

var (
	configFilename = flag.String("config", "config.yaml", "Configuration file in YAML")
	logLevelRaw    = flag.String("log-level", "info", "Level of logging")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.JSONFormatter{})

	logLevel, err := logrus.ParseLevel(*logLevelRaw)
	if err == nil {
		logrus.SetLevel(logLevel)
	}

	config, err := LoadConfig(*configFilename)
	if err != nil {
		log.Fatal(err)
	}

	logrus.Infof("Listening address: %s", config.Listen)
	bot, err := New(config)
	if err != nil {
		log.Fatal(err)
	}

	logrus.Fatal(bot.Run())
}
