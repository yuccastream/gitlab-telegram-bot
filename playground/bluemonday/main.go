package main

import (
	"fmt"

	"github.com/microcosm-cc/bluemonday"
)

func main() {
	p := bluemonday.NewPolicy()
	// https://core.telegram.org/bots/api#html-style
	allowedElements := []string{"br",
		"a",
		"u", "ins",
		"b", "strong",
		"i", "em",
		"s", "strike", "del",
		"span", "tg-spoiler",
		"code",
		"pre",
	}
	skipContentElements := []string{
		"details",
		"summary",
	}
	p.AllowStandardURLs()
	p.AllowElements(allowedElements...)
	p.AllowAttrs("href").Globally()
	p.SkipElementsContent(skipContentElements...)
	out := p.Sanitize(`Hello <details>Details<summary>Summary</summary></details> <a href="https://gitlab.com/yuccastream/yucca/-/pipelines/470503544">yuccastream/yucca</a>`)
	fmt.Println(out)
}
