package main

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

type Telegram interface {
	Send(c tgbotapi.Chattable) (tgbotapi.Message, error)
}
