package main

import (
	"reflect"
	"strings"

	"github.com/xanzy/go-gitlab"
)

type IssueEventWrapped gitlab.IssueEvent

type MergeEventWrapped gitlab.MergeEvent

func (i *IssueEventWrapped) ChangesList() (changes []string) {
	if i.ObjectAttributes.Action != "update" {
		return
	}
	description := i.Changes.Description
	if description.Previous != description.Current {
		changes = append(changes, "Description updated")
	}
	title := i.Changes.Title
	if title.Previous != title.Current {
		changes = append(changes, "Title updated")
	}
	labels := i.Changes.Labels
	if !reflect.DeepEqual(labels.Previous, labels.Current) {
		changes = append(changes, "Labels updated")
	}
	return
}

func (i *IssueEventWrapped) ChangesString() string {
	changes := i.ChangesList()
	return strings.Join(changes, ", ")
}

func (m *MergeEventWrapped) ChangesList() (changes []string) {
	if m.ObjectAttributes.Action != "update" {
		return
	}
	description := m.Changes.Description
	if description.Previous != description.Current {
		changes = append(changes, "Description updated")
	}
	title := m.Changes.Title
	if title.Previous != title.Current {
		changes = append(changes, "Title updated: %s → %s", title.Previous, title.Current)
	}
	labels := m.Changes.Labels
	if !reflect.DeepEqual(labels.Previous, labels.Current) {
		changes = append(changes, "Labels updated")
	}
	assignees := m.Changes.Assignees
	if !reflect.DeepEqual(assignees.Previous, assignees.Current) {
		changes = append(changes, "Assignees updated")
	}
	milestoneID := m.Changes.MilestoneID
	if milestoneID.Previous != milestoneID.Current {
		changes = append(changes, "Milestone updated")
	}
	sourceBranch := m.Changes.SourceBranch
	if sourceBranch.Previous != sourceBranch.Current {
		changes = append(changes, "Source branch updated: %s → %s", sourceBranch.Previous, sourceBranch.Current)
	}
	targetBranch := m.Changes.TargetBranch
	if targetBranch.Previous != targetBranch.Current {
		changes = append(changes, "Target branch updated: %s → %s", targetBranch.Previous, targetBranch.Current)
	}
	return
}

func (m *MergeEventWrapped) ChangesString() string {
	changes := m.ChangesList()
	return strings.Join(changes, ", ")
}
